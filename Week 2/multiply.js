// Instruction
// Create a function to multiply the inputs
// Your code should validate the input only accept the NUMBER type and cannot BLANK
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null
}

function del(ms) {
  return new Promise(
    resolve => setTimeout(resolve,ms)
  )
}

async function calculating(ms) {
  console.log("\n");
  for (i = 0 ; i < ms; i += 1000) {
    await del(i);
    console.log("Calculating.....")
  }
  console.log("\n");
}

async function generating(ms) {
  console.log("\n");
  for (i = 0 ; i < ms; i += 1000) {
    await del(i);
    console.log("Generating.....")
  }
  console.log("\n");
}

function ranNumGen() {
  num =  0 + Math.floor((100)*Math.random());
  return num
}

function genNum1() {
  rl.question("First Number: ", num1 => {
    if (!isNaN(num1) && !isEmptyOrSpaces(num1)) {
      genNum2(num1);
    } else {
      console.log("\nPlease key-in only number");
      genNum1();
    }
  })
}

function genNum2(num1) {
  rl.question("Second Number: ", num2 => {
    if (!isNaN(num2) && !isEmptyOrSpaces(num2)) {
      multiply(num1,num2);
    } else {
      console.log("\nPlease key-in only number");
      genNum2(num1);
    }
  })
}

async function multiply(num1,num2) {
  await calculating(2000)
  console.log("Result: "+ num1*num2);
  option();
}

function option() {
  console.log('\nPlease choose: \n1.Multiply random number from 0-100.\n2.Key-in the number.\n3.Quit.');
  rl.question("Input: ", async input => {
    if (input == 1) {
      await generating(4000)
      num1 = ranNumGen();
      console.log(`First Random Number Generated : ${num1}`);
      await generating(4000)
      num2 = ranNumGen();
      console.log(`Second Random Number Generated : ${num2}`)
      multiply(num1,num2);
    } else if (input ==2) {
      console.log("Please key-in two number for multiplication\n");
      genNum1();
    } else if (input == 3){
      rl.close();
    } else {
      console.log(`\nPlease key-in only 1, 2, or "q"`);
      option()
    }
  })
}

rl.on("close", () => {
  process.exit()
})

console.log("Multiplication of 2 numbers");
console.log("*****************************************\nPlease note that multiplication require\nfew seconds to process the answer\n*****************************************");
console.log("Please note that random number generation\nrequire few seconds to process the answer\n*****************************************");
option();

// example
// num1 = 2, num2 = 4, return 8
// num1 = "2", num2 = 4, return "Please provide a number only!"
// num1 = true, num2 = null, return "Please provide a number only!"
// num1 UNDEFINED, num2 = "2", return "Please provide a value!"
