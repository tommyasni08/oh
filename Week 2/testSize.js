// Write chained if/else if statements to fulfill the following conditions:
// num < 5 - return "Tiny"
// num < 10 - return "Small"
// num < 15 - return "Medium"
// num < 20 - return "Large"
// num >= 20 - return "Huge"
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})
function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}
function checkRange(num, min, max) {
    return min <= num && num <= max;
  }


function testSize(num) {
    if (num < 5) {
        return "Tiny"
    } else if (num < 10) {
        return "Small"
    } else if (num < 15) {
        return "Medium"
    } else if (num < 20) {
        return "Large"
    } else {
        return "Huge"
    }
}

function inputSize() {
    console.log('N: key-in "q" to quit');
    rl.question("Please key-in your size in number: ", score => {
    if (score == "q") {
        rl.close()
    } else if (!isNaN(score) && checkRange(score,0,100) && !isEmptyOrSpaces(score)) {
        console.log(`Your size is ${testSize(score)}\n`);
        inputSize();
    } else {
        console.log("\nPlease key-in only number within 0 - 100\n");
        inputSize();
    }})
}

rl.on("close", () => {
    process.exit()
})

inputSize()


// console.log(testSize(0));
// console.log(testSize(5));
// console.log(testSize(10));
// console.log(testSize(17));
// console.log(testSize(25));
// console.log(testScore(50));
// console.log(testScore(90));

//testSize(0) should return "Tiny"
//testSize(5) should return "Small"
//testSize(10) should return "Medium"
//testSize(17) should return "Large"
//testSize(25) should return "Huge"
