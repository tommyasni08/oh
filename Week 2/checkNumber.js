// Instruction
// Create a function that check if an input is a number
function checkNumber(input) {
  console.log((!isNaN(input) ? "TRUE" : "FALSE"));
}

checkNumber("A");
checkNumber(10);
// example
// input = "A" should return FALSE
// input = 10 should return TRUE
