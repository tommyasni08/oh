// Instruction
// Create a function to multiply the inputs
// Your code should validate the input only accept the NUMBER type and cannot BLANK

function multiply(num1,num2) {
  if ((typeof num1 == "number") && (typeof num2 == "number")) {
    console.log(num1 * num2);
  } else if ((typeof num1 === 'undefined') || (typeof num2 === 'undefined')) {
    console.log("Please provide a value!");
  } else {
    console.log("Please provide a number only!");
  }
}

var num; //Declare Undefined Variable

multiply(2,4);
multiply("2",4);
multiply(true,null);
multiply(num,"2");



// example
// num1 = 2, num2 = 4, return 8
// num1 = "2", num2 = 4, return "Please provide a number only!"
// num1 = true, num2 = null, return "Please provide a number only!"
// num1 UNDEFINED, num2 = "2", return "Please provide a value!"
