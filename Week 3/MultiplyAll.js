// const _ = require('lodash')
// const _ = require('underscore');

// function flatten(arr) {
//   return arr.reduce((acc,cur) => acc.concat(Array.isArray(cur) ? flatten(cur) : cur), [])
// }

function multiplyAll(arr) {

  // Only change code below this line
  //Method 1: Nested Foor loop

  // var product = 1;
  // for (let i=0; i<arr.length; i++) {
  //   for (let j=0; j<arr[i].length; j++){
  //     product = product * arr[i][j];
  //   }
  // }

  // Method 2: flatten nested array using JS Library (underscore)
  // const flattened = _.flatten(arr);
  // let product = flattened.reduce((total,value)=> total * value);

  // Method 3: flatten nested array using flat method
  const flattened = arr.flat(Infinity);
  let product = flattened.reduce((total,value)=> total * value);

  // Method 4 : Recursive using reduce method (require new function "flatten")
  // let flattened = flatten(arr);
  // let product = flattened.reduce((total,value)=> total * value);

  // Only change code above this line
  return product;
}

console.log(multiplyAll([[1,2],[3,4],[5,6,7]]));
//multiplyAll([[1,2],[3,4],[5,6,7]]) should return 5040
console.log(multiplyAll([[1,2],[3,[4,5]],[6,7,[[[8],9]]],10]));
//multiplyAll([[1,2],[3,[4,5]],[6,7,[[[8],9]]],10]) should return 3628800
